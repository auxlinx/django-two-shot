"""
Module Docstring
"""

from django import forms
from .models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    """
    Class Docstring
    """

    class Meta:
        """
        Class Meta Docstring
        """

        model = Receipt
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        """
        Method Docstring
        """
        receipt = super().save(commit=False)
        if commit:
            receipt.save()
        return receipt


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
