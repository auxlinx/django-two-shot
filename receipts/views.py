from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def receipts(request):
    user_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": user_receipts,
    }
    return render(request, "receipt/receipt_list.html", context)


@login_required
def user_create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            user_receipt = form.save(False)
            user_receipt.purchaser = request.user
            user_receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "user_receipt": form,
    }

    return render(request, "receipt/create_receipt.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/expenses_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "create_category": form,
    }
    return render(request, "categories/create_expenses.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/accounts_list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "create_account": form,
    }
    return render(request, "accounts/create_accounts.html", context)
