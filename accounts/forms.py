from django import forms


class Login(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())


class Sign_up(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput()
    )

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")

        if password != password:
            raise forms.ValidationError("The passwords do not match")
